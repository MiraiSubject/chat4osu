export class CyclePreviousHistory {
  static readonly type = '[History] CyclePreviousHistory';
}

export class CycleNextHistory {
  static readonly type = '[History] CycleNextHistory';
}
